﻿using System;
using System.IO;
using WebSocketSharp;

namespace Anima.Orium {

    public class OriumWSSTransportLayer : IOriumTransportLayer 
    {
        private WebSocket _socket = null;
        private bool _connected = false;

        public event EventHandler           Connected;
        public event MessageEventHandler    MessageReceived;
        public event EventHandler           Disconnected;
        public event ErrorEventHandler      ConnectionError;

        public bool IsConnected
        {
            get { return _connected; }
        }

        public OriumWSSTransportLayer() { }

        public OriumWSSTransportLayer(WebSocket socket)
        {
            _socket = socket;

            _socket.OnOpen += OnOpen;
            _socket.OnMessage += OnMessage;
            _socket.OnClose += OnClose;
            _socket.OnError += OnError;

            OnOpen(this, new EventArgs());
        }

        public void Connect(string address)
        {
            _socket = new WebSocket(address, new String[0]);

            _socket.OnOpen += OnOpen;
            _socket.OnMessage += OnMessage;
            _socket.OnClose += OnClose;
            _socket.OnError += OnError;

            _socket.ConnectAsync();
        }

        public void Disconnect()
        {
            if(_socket != null)
            {
                _socket.CloseAsync(CloseStatusCode.Normal);
            }
        }

        public void Send(byte[] data)
        {
            if (_socket != null)
            {
                _socket.SendAsync(data, (Action<bool>) delegate(bool success) {});
            }
        }

        private void OnOpen(object sender, EventArgs args)
        {
            _connected = true;

            if(Connected != null)
            {
                Connected(this, args);
            }
        }

        private void OnMessage(object sender, WebSocketSharp.MessageEventArgs args)
        {
            if (MessageReceived != null)
            {
                MessageReceived(this, args.RawData);
            }
        }

        private void OnClose(object sender, EventArgs args)
        {
            _socket = null;
            _connected = false;

            if (Disconnected != null)
            {
                Disconnected(this, args);
            }
        }

        private void OnError(object sender, WebSocketSharp.ErrorEventArgs args)
        {
            if (ConnectionError != null)
            {
                ConnectionError(this, new System.IO.ErrorEventArgs(args.Exception));
            }
        }
    }

}
