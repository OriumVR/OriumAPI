﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Anima.Orium {

    public class OriumRegistry {
        
        private Dictionary<ulong, Type> _hashTypes = new Dictionary<ulong, Type>();
        private Dictionary<ulong, double> _hashPriorities = new Dictionary<ulong, double>();
        private Dictionary<ulong, OriumBaseSerializer> _hashInstances = new Dictionary<ulong, OriumBaseSerializer>();

        private Dictionary<Type,ulong> _typeHashes = new Dictionary<Type,ulong>();

        public bool IsSerializerType(Type type)
        {
            return type.IsSubclassOf(typeof(OriumBaseSerializer))
                && type.GetConstructor(Type.EmptyTypes) != null
                && !type.IsAbstract;
        }

        /// <summary>
        /// Registers a message serializer.
        /// </summary>
        public bool RegisterSerializer(Type serializerType)
        {
            if (!IsSerializerType(serializerType))
                throw new ArgumentException("Type is not a valid OriumBaseSerializer");

            if (_typeHashes.ContainsKey(serializerType))
                return false;

            OriumBaseSerializer serializer = (OriumBaseSerializer)Activator.CreateInstance(serializerType);
            ulong idHash = OriumUtils.CalculateHash (serializer.MessageId);

            _hashTypes.Add(idHash, serializerType);
            _hashInstances.Add(idHash, serializer);
            _typeHashes.Add(serializerType, idHash);

            RegisterSerializerPriority(idHash, serializer.Priority);

            return true;
        } 

        public void RegisterSerializerPriority(ulong idHash, double priority)
        {
            if(!_hashPriorities.ContainsKey(idHash))
            {
                _hashPriorities.Add(idHash, priority);
            }
        }

        public void UnregisterSerializer(Type serializerType)
        {
            ulong idHash;
            if (!_typeHashes.TryGetValue(serializerType, out idHash))
                return;

            _hashTypes.Remove(idHash);
            _hashInstances.Remove(idHash);
            _typeHashes.Remove(serializerType);
        }

        public ulong GetSerializerHash(Type serializerType)
        {
            ulong hash = 0;
            if (_typeHashes.TryGetValue(serializerType, out hash))
            {
                return hash;
            }
            return 0;
        }

        public ICollection<ulong> GetHashes()
        {
            return _hashPriorities.Keys;
        }

        public double GetSerializerPriority(ulong hash)
        {
            double priority = 1.0;
            if (_hashPriorities.TryGetValue(hash, out priority))
            {
                return priority;
            }
            return 1.0;
        }

        public bool GetSerializerCanLoopback(ulong hash)
        {
            OriumBaseSerializer instance = null;
            if (_hashInstances.TryGetValue(hash, out instance))
            {
                return instance.AllowLoopback;
            }
            return false;
        }

        public bool HasSerializer(Type serializerType)
        {
            return _typeHashes.ContainsKey(serializerType);
        }

        public bool HasSerializer(ulong hash)
        {
            return _hashTypes.ContainsKey(hash);
        }


        public void Serialize<TYPE>(object packet, BinaryWriter bw) where TYPE : OriumBaseSerializer, new()
        {
            ulong hash;
            if(_typeHashes.TryGetValue(typeof(TYPE), out hash))
            {
                Serialize(hash, packet, bw);
            }
        }

        public void Serialize(ulong hash, object packet, BinaryWriter bw)
        {
            OriumBaseSerializer serializer;

            if (_hashInstances.TryGetValue(hash, out serializer))
            {
                serializer.Serialize(packet, bw);
            }
        }

        public bool Deserialize(BinaryReader br, ulong hash, out object packet)
        {
            OriumBaseSerializer serializer;

            if (_hashInstances.TryGetValue(hash, out serializer))
            {
                long tmpPosition = br.BaseStream.Position;

                packet = serializer.Deserialize(br);

                br.BaseStream.Position = tmpPosition;

                return true;
            }
            else
            {
                packet = null;
                return false;
            }
        }
    }

}
