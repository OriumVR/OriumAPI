﻿using System;
using System.IO;

namespace Anima.Orium {

    public delegate void MessageEventHandler(object sender, byte[] data);

    public interface IOriumTransportLayer 
    {
        event EventHandler          Connected;
        event MessageEventHandler   MessageReceived;
        event EventHandler          Disconnected;
        event ErrorEventHandler     ConnectionError;

        bool IsConnected
        {
            get;
        }

        void Connect(string address);
        void Disconnect();
        void Send(byte[] data);
    }

}
