﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anima.Orium
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOriumServerPlugin
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="protocol"></param>
        void OnClientConnected(OriumProtocol protocol);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="protocol"></param>
        void OnClientDisconnected(OriumProtocol protocol);
    }
}
