﻿
namespace Anima.Orium {

    public class OriumUtils {

        public static ulong CalculateHash(string read)
        {
            // http://stackoverflow.com/questions/9545619/a-fast-hash-function-for-string-in-c-sharp

            ulong hashedValue = 3074457345618258791ul;
            for(int i=0; i<read.Length; i++)
            {
                hashedValue += read[i];
                hashedValue *= 3074457345618258799ul;
            }
            return hashedValue;
        }

    }

}
