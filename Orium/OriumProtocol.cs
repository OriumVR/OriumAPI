﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Anima.Orium
{

    public delegate void OriumCallback(long timestamp, ulong channel, object packet);
    public delegate void OriumFallback(long timestamp, ulong hash, ulong channel, byte[] packet);

    public class OriumProtocol
    {
        const int DEFAULT_SYNC_TIMESTAMPS = 10;
        const int DEFAULT_LAG_TIMESTAMPS = 50;
        const double MAX_ACCEPTABLE_LAG_TIME = 2.0;

        private IOriumTransportLayer _transport = null;
        private OriumRegistry _registry;
        private Random _random = new Random();
        private long _timeDifference = 0L;
        private bool _isServer = false;
        private bool _synchronizing = true;

        // Initial Synchronization
        private int _syncTimestampsToSend = 0;
        private bool _syncTimestampExpected = false;

        private List<long> _syncSendTimes = new List<long>(DEFAULT_SYNC_TIMESTAMPS);
        private List<long> _syncReceiveTimes = new List<long>(DEFAULT_SYNC_TIMESTAMPS);
        private List<long> _syncTimestampsReceived = new List<long>(DEFAULT_SYNC_TIMESTAMPS);

        // Moving Average
        private long _lagTimeAccum = 0L;
        public long _lagTimeAverage = 0L;
        public long _lagTimeVelocity = 0L;
        private int _lagTimestampsCounted = 0;

        private long _stopFlushingTime = long.MaxValue;

        private Dictionary<ulong, List<OriumCallback>> _hashCallbacks = 
            new Dictionary<ulong, List<OriumCallback>>();
        private Dictionary<ulong, Dictionary<ulong, List<OriumCallback>>> _hashChannelCallbacks = 
            new Dictionary<ulong, Dictionary<ulong, List<OriumCallback>>>();
        private List<OriumFallback> _fallbacks = new List<OriumFallback>();

        public double PacketDropRatio = 0.0;
        public double OtherPacketDropRatio = 0.0;
        public bool Flushing
        {
            get { return _stopFlushingTime != long.MaxValue; }
        }

        public event EventHandler Connected;
        public event ErrorEventHandler ConnectionError;
        public event EventHandler Disconnected;

        public OriumRegistry Registry {
            get { return _registry; }
            set { _registry = value; }
        }

        private long GetTime()
        {
            return DateTime.Now.Ticks;
        }

        public long GetServerTime()
        {
            return DateTime.Now.Ticks + (_isServer ? 0L : _timeDifference);
        }

        public OriumProtocol(OriumRegistry registry, IOriumTransportLayer transport)
        {
            if (registry != null)
            {
                _registry = registry;

                _registry.RegisterSerializer(typeof(OriumPacketDropSerializer));
                _registry.RegisterSerializer(typeof(OriumPacketPrioritySerializer));

                ulong packetDropHash = _registry.GetSerializerHash(typeof(OriumPacketDropSerializer));
                ulong packetPriorityHash = _registry.GetSerializerHash(typeof(OriumPacketPrioritySerializer));

                _hashCallbacks.Add(packetDropHash, new List<OriumCallback>());
                _hashCallbacks.Add(packetPriorityHash, new List<OriumCallback>());

                _hashCallbacks[packetDropHash].Add(OnPacketDropPacket);
                _hashCallbacks[packetPriorityHash].Add(OnPacketPriorityPacket);
            }

            if (transport != null)
            {
                _transport = transport;

                if (_transport.IsConnected)
                {
                    _isServer = true;
                    OnConnected(null, new EventArgs());
                }

                _transport.Connected        += OnConnected;
                _transport.MessageReceived  += OnMessageReceived;
                _transport.Disconnected     += OnDisconnected;
                _transport.ConnectionError  += OnConnectionError;
            }
        }

        public void Connect(string address)
        {
            _transport.Connect(address);
        }

        public void Disconnect()
        {
            _transport.Disconnect();
        }

        public void Send(Type type, object packet, ulong channelHash = 0, long originTimestamp = 0L)
        {
            if (!_registry.IsSerializerType(type))
                throw new ArgumentException("Type is not a valid OriumBaseSerializer");

            bool newSerializer = _registry.RegisterSerializer(type);
            ulong typeHash = _registry.GetSerializerHash(type);

            if (newSerializer)
            {
                Send(typeof(OriumPacketPrioritySerializer), new PacketPriorityPacket 
                {
                    Hash = typeHash,
                    Priority = _registry.GetSerializerPriority(typeHash)
                });
            }

            Send(typeHash, packet, channelHash, originTimestamp);

            if (_registry.GetSerializerCanLoopback(typeHash))
            {
                Callback(packet, typeHash, GetServerTime(), channelHash);
            }
        }

        public void Send(ulong typeHash, object packet, ulong channelHash = 0, long originTimestamp = 0L)
        {
            if (_transport == null || _synchronizing)
                return;

            if (PacketDropRatio > 0.0)
            {
                double priority = _registry.GetSerializerPriority(typeHash);
                double dropProbability = PacketDropRatio * (1.0 - priority);

                if (_random.NextDouble() < dropProbability)
                {
                    return;
                }
            }

            using (MemoryStream ms = new MemoryStream())
            {
                using (BinaryWriter bw = new BinaryWriter(ms))
                {
                    long timestamp = GetServerTime();

                    if (originTimestamp == 0L)
                    {
                        originTimestamp = timestamp;
                    }
                    
                    // Neighbour Timestamp
                    bw.Write(timestamp);
                    // Origin Timestamp
                    bw.Write(originTimestamp);

                    // Type Hash
                    bw.Write(typeHash);
                    // Channel Hash
                    bw.Write(channelHash);

                    _registry.Serialize(typeHash, packet, bw);

                    _transport.Send(ms.ToArray());
                }
            }
        }

        public void Send(ulong typeHash, byte[] data, ulong channelHash = 0, long originTimestamp = 0L)
        {          
            if (_transport == null || _synchronizing)
                return;
            
            if (PacketDropRatio > 0.0)
            {
                double priority = _registry.GetSerializerPriority(typeHash);
                double dropProbability = PacketDropRatio * (1.0 - priority);

                if (_random.NextDouble() < dropProbability)
                {
                    return;
                }
            }

            using (MemoryStream ms = new MemoryStream())
            {
                using (BinaryWriter bw = new BinaryWriter(ms))
                {
                    long timestamp = GetServerTime();

                    if(originTimestamp == 0L)
                    {
                        originTimestamp = timestamp;
                    }

                    // Neighbour Timestamp
                    bw.Write(timestamp);
                    // Origin Timestamp
                    bw.Write(originTimestamp);

                    // Type Hash
                    bw.Write(typeHash);
                    // Channel Hash
                    bw.Write(channelHash);

                    bw.Write(data);

                    _transport.Send(ms.ToArray());
                }
            }
        }

        public void RegisterCallback(Type serializerType, OriumCallback callback, ulong channelHash = 0)
        {
            if (!_registry.IsSerializerType(serializerType))
                throw new ArgumentException("Type is not a valid OriumBaseSerializer");

            bool newSerializer = _registry.RegisterSerializer(serializerType);
            ulong typeHash = _registry.GetSerializerHash(serializerType);
            if (newSerializer && _transport != null && !_synchronizing)
            {
                Send(typeof(OriumPacketPrioritySerializer), new PacketPriorityPacket
                { 
                    Hash = typeHash,
                    Priority = _registry.GetSerializerPriority(typeHash)
                });
            }

            List<OriumCallback> callbacks;

            if (channelHash != 0)
            {
                Dictionary<ulong,List<OriumCallback>> channels;
                if (!_hashChannelCallbacks.TryGetValue(typeHash, out channels))
                {
                    channels = new Dictionary<ulong,List<OriumCallback>>();
                    _hashChannelCallbacks.Add(typeHash, channels);
                }

                if (!channels.TryGetValue(channelHash, out callbacks))
                {
                    callbacks = new List<OriumCallback>();
                    channels.Add(channelHash, callbacks);
                }
            }
            else
            {
                if (!_hashCallbacks.TryGetValue(typeHash, out callbacks))
                {
                    callbacks = new List<OriumCallback>();
                    _hashCallbacks.Add(typeHash, callbacks);
                }
            }

            callbacks.Add(callback);
        }

        public void UnregisterCallback(Type serializerType, OriumCallback callback, ulong channelHash = 0)
        {
            ulong typeHash = _registry.GetSerializerHash(serializerType);

            List<OriumCallback> callbacks;

            if (channelHash != 0)
            {
                Dictionary<ulong,List<OriumCallback>> channels;
                if (_hashChannelCallbacks.TryGetValue(typeHash, out channels))
                {
                    if (channels.TryGetValue(channelHash, out callbacks))
                    {
                        callbacks.Remove(callback);

                        if (callbacks.Count < 1)
                        {
                            channels.Remove(channelHash);

                            if (channels.Count < 1)
                            {
                                _hashChannelCallbacks.Remove(typeHash);
                            }
                        }
                    }
                }
            }
            else
            {
                if (_hashCallbacks.TryGetValue(typeHash, out callbacks))
                {
                    callbacks.Remove(callback);

                    if (callbacks.Count < 1)
                    {
                        _hashCallbacks.Remove(typeHash);
                    }
                }
            }
        }

        public void RegisterFallback(OriumFallback fallback)
        {
            _fallbacks.Add(fallback);
        }

        public void Update()
        {
            if (GetTime() >= _stopFlushingTime)
            {
                Send(typeof(OriumPacketDropSerializer), new PacketDropPacket(OtherPacketDropRatio));
                _stopFlushingTime = long.MaxValue;
            }
        }

        private void SendSyncTimestamp()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (BinaryWriter bw = new BinaryWriter(ms))
                {
                    long now = GetTime();
                    bw.Write(now);

                    _transport.Send(ms.ToArray());

                    _syncSendTimes.Add(now);
                    _syncTimestampExpected = true;
                    _syncTimestampsToSend--;
                }
            }

            if(_isServer && _syncTimestampsToSend == 0)
            {
                FinalizeSync();
            }
        }

        private void OnSyncTimestampReceived(long timestamp)
        {
            _syncReceiveTimes.Add(GetTime());
            _syncTimestampsReceived.Add(timestamp);

            if (_syncTimestampsToSend > 0)
            {
                SendSyncTimestamp();
            }
            else if(!_isServer)
            {
                FinalizeSync();

                // Register every packet type with the server
                foreach(ulong hash in _registry.GetHashes())
                {
                    Send(typeof(OriumPacketPrioritySerializer), new PacketPriorityPacket
                    {
                        Hash = hash,
                        Priority = _registry.GetSerializerPriority(hash)
                    });
                }
            }
        }

        private void FinalizeSync()
        {
            _syncTimestampExpected = false;

            long diffAccumulator = 0L;

            for (int i = 0; i < DEFAULT_SYNC_TIMESTAMPS; ++i)
            {
                long localTimeAtServerSend = _syncSendTimes[i] +
                    ((_syncReceiveTimes[i] - _syncSendTimes[i]) / 2);

                diffAccumulator += _syncTimestampsReceived[i] - localTimeAtServerSend;
            }

            _timeDifference = diffAccumulator / DEFAULT_SYNC_TIMESTAMPS;

            Console.WriteLine("Time Difference: " + TimeSpan.FromTicks(_timeDifference).TotalSeconds);

            _synchronizing = false;

            Connected?.Invoke(this, new EventArgs());
        }

        // This doesn't work, because if the lag time reaches the lower bound,
        // and the packet drop ratio is high, the velocity will likely go up, 
        // so the ratio will be unlilely to ever go down again
        private void UpdatePacketDroppingByVelocity(long lastLagTime)
        {
            _lagTimeVelocity = _lagTimeAverage - lastLagTime;

            if (_lagTimeAverage > TimeSpan.FromSeconds(MAX_ACCEPTABLE_LAG_TIME).Ticks)
            {
                _stopFlushingTime = GetTime() + _lagTimeAverage;
                // FLUSH
            }

            Console.WriteLine("Lag Time Average: " + TimeSpan.FromTicks(_lagTimeAverage).TotalSeconds +
                                " Lag Time Velocity: " + TimeSpan.FromTicks(_lagTimeVelocity).TotalSeconds);

            if (_lagTimeVelocity > 0 && OtherPacketDropRatio < 1.0)
            {
                // INDUCE PACKET DROPPING
                OtherPacketDropRatio += 0.05;
            }
            if (_lagTimeVelocity < 0 && OtherPacketDropRatio > 0.0)
            {
                OtherPacketDropRatio -= 0.05;
            }
        }

        private void UpdatePacketDroppingByAsymptote()
        {
            double lagLevel = Math.Max(0.0, TimeSpan.FromTicks(_lagTimeAverage).TotalSeconds / MAX_ACCEPTABLE_LAG_TIME);

            Console.WriteLine("Lag Time Average: " + TimeSpan.FromTicks(_lagTimeAverage).TotalSeconds +
                                " Lag Level: " + lagLevel);

            if(lagLevel > 1.0)
            {
                _stopFlushingTime = GetTime() + _lagTimeAverage;
                // FLUSH
            }
            else
            {
                OtherPacketDropRatio = lagLevel;
            }
        }

        private void PerformLagChecks(long timestamp)
        {
            long lagTime = GetServerTime() - timestamp;

            _lagTimeAccum += lagTime;

            _lagTimestampsCounted++;

            if (_lagTimestampsCounted >= DEFAULT_LAG_TIMESTAMPS)
            {
                long lastLagTime = _lagTimeAverage;

                _lagTimeAverage = _lagTimeAccum / DEFAULT_LAG_TIMESTAMPS;

                _lagTimeAccum = 0;
                _lagTimestampsCounted = 0;

                UpdatePacketDroppingByAsymptote();

                if(_stopFlushingTime < long.MaxValue)
                {
                    Send(typeof(OriumPacketDropSerializer), new PacketDropPacket(0.0));
                }
                else
                {
                    Send(typeof(OriumPacketDropSerializer), new PacketDropPacket(OtherPacketDropRatio));
                }
            }
        }

        private void OnConnectionError(object sender, ErrorEventArgs e)
        {
            ConnectionError?.Invoke(this, e);
        }

        private void OnDisconnected(object sender, EventArgs e)
        {
            Disconnected?.Invoke(this, new EventArgs());
        }

        private void OnMessageReceived(object sender, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            {
                using (BinaryReader br = new BinaryReader(ms))
                {
                    long neighborTimestamp = br.ReadInt64();

                    if (_syncTimestampExpected)
                    {
                        OnSyncTimestampReceived(neighborTimestamp);
                    }
                    else if (!Flushing)
                    {
                        PerformLagChecks(neighborTimestamp);
                    }

                    if (ms.Position < ms.Length)
                    {
                        long originTimestamp = br.ReadInt64();

                        ulong typeHash = br.ReadUInt64();
                        ulong channelHash = br.ReadUInt64();

                        object packet;

                        try
                        {
                            if (_registry.Deserialize(br, typeHash, out packet))
                            {
                                Callback(packet, typeHash, originTimestamp, channelHash);
                            }
                            else
                            {
                                foreach (OriumFallback fallback in _fallbacks)
                                {
                                    long position = br.BaseStream.Position;
                                    fallback(originTimestamp, typeHash, channelHash, br.ReadBytes((int)(br.BaseStream.Length - br.BaseStream.Position)));
                                    br.BaseStream.Position = position;
                                }
                            }
                        }
                        catch(Exception)
                        {
                            foreach (OriumFallback fallback in _fallbacks)
                            {
                                long position = br.BaseStream.Position;
                                fallback(originTimestamp, typeHash, channelHash, br.ReadBytes((int)(br.BaseStream.Length - br.BaseStream.Position)));
                                br.BaseStream.Position = position;
                            }
                        }
                    }
                }
            }
        }

        private void Callback(object packet, ulong typeHash, long originTimestamp, ulong channelHash = 0)
        {
            List<OriumCallback> callbacks;

            // Should callbacks with a channelHash of 0 accept all channels???
            if (channelHash == 0)
            {
                if (_hashCallbacks.TryGetValue(typeHash, out callbacks))
                {
                    foreach (OriumCallback callback in callbacks)
                    {
                        callback(originTimestamp, channelHash, packet);
                    }
                }
                else
                {
                    using (MemoryStream ms = new MemoryStream())
                    using (BinaryWriter bw = new BinaryWriter(ms))
                    {
                        _registry.Serialize(typeHash, packet, bw);

                        foreach (OriumFallback fallback in _fallbacks)
                        {
                            fallback(originTimestamp, typeHash, channelHash, ms.ToArray());
                        }
                    }
                }
            }
            else
            {
                Dictionary<ulong, List<OriumCallback>> channels;

                if (_hashChannelCallbacks.TryGetValue(typeHash, out channels) &&
                    channels.TryGetValue(channelHash, out callbacks))
                {
                    foreach (OriumCallback callback in callbacks)
                    {
                        callback(originTimestamp, channelHash, packet);
                    }
                }
                else
                {
                    using (MemoryStream ms = new MemoryStream())
                    using (BinaryWriter bw = new BinaryWriter(ms))
                    {
                        _registry.Serialize(typeHash, packet, bw);

                        foreach (OriumFallback fallback in _fallbacks)
                        {
                            fallback(originTimestamp, typeHash, channelHash, ms.ToArray());
                        }
                    }
                }
            }
        }

        private void OnConnected(object sender, EventArgs args)
        {
            _synchronizing = true;

            if (_isServer)
            {
                _syncTimestampExpected = true;
                _syncTimestampsToSend = DEFAULT_SYNC_TIMESTAMPS;
            }
            else
            {
                SendSyncTimestamp();
                _syncTimestampsToSend = DEFAULT_SYNC_TIMESTAMPS - 1;
            }
        }

        private void OnPacketDropPacket(long timestamp, ulong channel, object packet)
        {
            Console.WriteLine("Packet Drop Ratio: " + ((PacketDropPacket)packet).Ratio);
            PacketDropRatio = ((PacketDropPacket)packet).Ratio;
        }

        private void OnPacketPriorityPacket(long timestamp, ulong channel, object packet)
        {
            PacketPriorityPacket priorityPacket = (PacketPriorityPacket)packet;
            _registry.RegisterSerializerPriority(priorityPacket.Hash, priorityPacket.Priority);
        }
    }

}
