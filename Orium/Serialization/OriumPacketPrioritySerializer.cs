﻿using System.Collections;
using System.IO;

namespace Anima.Orium
{
    public class PacketPriorityPacket
    {
        public ulong Hash;
        public double Priority;
    }

    public class OriumPacketPrioritySerializer : OriumBaseSerializer 
    {
        public override string MessageId {
            get { return "Orium.PacketPriority"; }
        }

        public override double Priority {
            get { return 1.0; }
        }

        public override void Serialize(object packet, BinaryWriter bw)
        {
            PacketPriorityPacket priorityPacket = (PacketPriorityPacket)packet;
            bw.Write(priorityPacket.Hash);
            bw.Write(priorityPacket.Priority);
        }

        public override object Deserialize(BinaryReader br)
        {
            return new PacketPriorityPacket
            {
                Hash = br.ReadUInt64(),
                Priority = br.ReadDouble()
            };
        }
    }

}
