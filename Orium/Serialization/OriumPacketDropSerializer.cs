﻿using System.Collections;
using System.IO;
using Anima.Orium;

namespace Anima.Orium {

    public class PacketDropPacket
    {
        public double Ratio = 0.0;

        public PacketDropPacket(double ratio)
        {
            Ratio = ratio;
        }
    }

    public class OriumPacketDropSerializer : OriumBaseSerializer {

        public override string MessageId {
            get { return "Orium.DropPackets"; }
        }

        public override double Priority {
            get { return 1.0; }
        }

        public override void Serialize(object packet, BinaryWriter bw)
        {
            bw.Write(((PacketDropPacket)packet).Ratio);
        }

        public override object Deserialize(BinaryReader br)
        {
            return new PacketDropPacket(br.ReadDouble());
        }
    }

}
