﻿using System;
using System.Collections;
using System.IO;

namespace Anima.Orium {

    public abstract class OriumBaseSerializer {

        public abstract string MessageId 
        {
            get;
        }

        public virtual double Priority
        {
            get { return 1.0; }
        }

        public virtual bool AllowLoopback
        {
            get { return false; }
        }

        public abstract void Serialize(object packet, BinaryWriter bw);

        public abstract object Deserialize(BinaryReader br);

    }

}
